<p align="center">
    <img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/logo.png" />
</p>
<h2 align="center">:tw-1f525: OneKeyAdmin 开源免费商用</h2>
<p align="center">
    <a href="http://www.onekeyadmin.com" target="_blank">
        <img src="https://img.shields.io/badge/OfficialWebsite-OneKeyAdmin-yellow.svg" />
    </a>
    <a href="http://www.onekeyadmin.com" target="_blank">
        <img src="https://img.shields.io/badge/Licence-Apache2.0-green.svg?style=flat" />
    </a>
    <a href="http://www.onekeyadmin.com" target="_blank">
        <img src="https://img.shields.io/badge/Edition-1.2.7-blue.svg" />
    </a>
     <a href="http://www.onekeyadmin.com/version.html" target="_blank">
        <img src="https://img.shields.io/badge/download-7.0m-red.svg" />
    </a>
</p>
<p align="center">    
    <b>如果对您有帮助，点右上角 "Star" 支持一下 谢谢！</b>
</p>

## 导航栏目
=========================================================

<a href="http://www.onekeyadmin.com/docs" target="_blank">开发文档&nbsp;&nbsp;</a>
<a href="http://www.onekeyadmin.com/question.html" target="_blank">社区地址&nbsp;&nbsp;</a>
<a href="http://www.onekeyadmin.com" target="_blank">官网地址&nbsp;&nbsp;</a>
<a href="http://gitee.onekeyadmin.com/mk_admin" target="_blank">演示地址&nbsp;&nbsp;</a>
<a href="http://www.onekeyadmin.com/api/system/version.html" target="_blank">立即下载</a>

=========================================================

OneKeyAdmin基于ThinkPHP+Element的低代码后台开发框架，开箱即用无需配置。填写参数即可完成CRUD，拥有开发助手选择单或多个数据表生成一个完整应用，包含控制器、模型、视图、菜单权限、API接口，让你的开发效率翻倍。

## 开发助手

<img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/curd.gif"/>

## 代码格式

<img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/curd.png"/>

## 主要特性

* php7.1+环境
* 基于Auth验证的权限管理系统

* 支持无限级父子级权限继承，父级的管理员可任意增删改子级管理员及权限设置
* 支持单管理员多角色
* 支持管理子级数据或个人数据
* 强大的开发助手

* 选择单或多个数据表一键生成完整应用插件，包含控制器、模型、视图、菜单权限、API接口，让你的开发效率翻倍
* 一键生成代码只需配置组件参数，json格式代码清晰易读
* 一键生成控制器菜单和规则
* 一键生成API接口
* 完善的前端功能组件开发

* 基于ELment.ui开发，自适应手机、平板、PC
* 基于Vue.js进行组件化管理，按需调用
* 强大的应用插件扩展功能，在线安装卸载升级应用插件
* 强大的主题模板功能，在线安装卸载，随心切换心仪模板
* 通用的会员模块和API模块
* 强大的路由机制，让搜索引擎快速收录
* 多语言数据独立、路由优雅
* 支持文件上传、水印、回收站、下载、检索、排序、编辑、列表切换、滚动加载
* 支持无限级分类、TDK设置、自定义字段、自定义路由、自定义页面、会员权限控制等等；
* 支持手机端访问后台管理系统，界面优雅，体验丝滑
* 强大的应用模块支持(CMS系统、博客系统、tinymce编辑器、流量统计、表单可视化管理、客服系统、电子画册、阿里云短信、支付（微信支付宝）、本地代码编辑、数据库备份、SEO网站优化、禁* 止指定IP访问、微信公众号登录、会员签到、CSDN采集、PHP中文网采集)
* 整合第三方阿里云短信接口
* 无缝整合第三方阿里云OSS云存储功能
* 第三方富文本编辑器支持
* 第三方登录(QQ、微信、微博)整合
* 第三方支付(微信、支付宝)无缝整合，微信支持PC端扫码支付
* 丰富的插件应用市场

## 在线演示

* <a href="http://gitee.onekeyadmin.com/mk_admin" target="_blank">无插件演示</a>
* <a href="http://cms.onekeyadmin.com/mk_admin" target="_blank">cms应用插件演示</a>
* <a href="http://blog.onekeyadmin.com/mk_admin" target="_blank">博客应用插件演示</a>
* <a href="http://swoole.onekeyadmin.com/mk_admin" target="_blank">swoole客服应用插件演示</a>

## 一键安装

* 运行环境要求PHP7.1+；
* 访问localhost/onekeyadmin/public输入数据库信息以完成安装；

## 系统截图

<table>
    <tr>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/1.jpg"/></td>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/2.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/3.jpg"/></td>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/4.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/6.jpg"/></td>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/7.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/8.jpg "/></td>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/9.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/10.jpg"/></td>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/11.jpg"/></td>
    </tr>   
    <tr>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/12.jpg"/></td>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/13.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/15.jpg"/></td>
        <td><img src="https://gitee.com/keheying/onekeyadmin/raw/master/readme/16.jpg"/></td>
    </tr>
</table>

## 兼容手机端

![截图](https://gitee.com/keheying/onekeyadmin/raw/master/readme/17.png)

## 开源版使用须知

1.允许用于个人学习、毕业设计、教学案例、公益事业、商用;

2.如果商用必须保留版权信息，请自觉遵守;

3.系统允许个人或公司进行任意二开及商用需要遵守Apache2.0开源协议的有关要求。

4.我们仅提供系统，运营内容与我们无关，请合法使用。

## 版权信息

遵循Apache2开源协议发布，并提供免费使用。

未经版权所有者明确授权，禁止发行本文档及其被实质上修改的版本。 

未经版权所有者明确授权，禁止去除后台管理系统版权标识。

版权所有Copyright © 2020-2023 by OneKeyAdmin (http://www.onekeyadmin.com)

All rights reserved。

## 官方QQ群

加入QQ交流群：648788102